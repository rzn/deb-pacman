# Deb Pacman v1.0
[**Click to DOWNLOAD!**](https://gitlab.com/TriVoxel/deb-pacman/uploads/9f716f95c66cd90e65157773d6dea05b/deb-pacman_1.0-0.deb)
> **Contents:**
>*  [Overview](#overview)
>*  [Installation](#installation)
>*  [Features](#features)
>    *  [Supported operations](#supported-operations)
>    *  [Supported options](#supported-options)
>*  [Limitations](#limitations)

## Overview:
Emulates the Archlinux Pacman package manager feel for Debian users who may prefer the style of Pacman over Apt. This program does not require any additional dependencies! Whoot! Don't expect all features to be added because Apt simply doesn't support all Pacman features and some Pacman features would be too tedious to replicate anyways. Casual users should hopefully find no trouble with the lack of features as all the most common Pacman functionality is present.

## Installation:
To install via your package manager, download the latest [release](https://gitlab.com/TriVoxel/deb-pacman/-/releases) which will be in the `.deb` file format. To manually install, simply [download the `pacman` file](https://gitlab.com/TriVoxel/deb-pacman/raw/master/pacman?inline=false) and make `pacman` executable. Then, copy it to `/usr/bin/`. Now you can use it normally in the terminal!

_Pro tip: Once this is installed, you can install `.deb` packages with `pacman` by using `# pacman -S <option> /path/to/file.deb`_

## Features:

### Supported operations:
    pacman {-h --help}
    pacman {-V --version}
    pacman {-R --remove}   [options] <package(s)>
    pacman {-S --sync}     [options] [package(s)]
    pacman {-U --upgrade}  [options] <file(s)>

### Supported options:
    pacman <operation>     {--noconfirm} [package(s)]

_Note: This is the only supported operation in this version of "Deb Pacman"_

_Pro tip: Use `# pacman -Rns <package(s)>` to remove orphaned packages (basically apt autoremove)!_

## Limitations:
You can only select eight (8) packages to install at once. If you use the argument `--noconfirm` you are limited to seven (7) packages. Also, the formatting of Deb Pacman is strict. It must be `$ pacman` followed by an operation ie `-Syyu` or `-R`, followed by an optional `--noconfirm` followed by the packages. Everything after this is functionally identical to Pacman!

```
 .--.                 TriVoxel's "Deb Pacman"
/ _.-' .-.  .-.  .-.  This software is not made or endorsed
\  '-. '-'  '-'  '-'  by Archlinux or the Pacman dev team
 '--'
                      This program may be freely redistributed under
                      the terms of the non-restrictive MIT license.
```